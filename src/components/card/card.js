import React from "react"

const Card = props => (
    <div className="card">
        <span className="rank">{props.rank}</span>
        <span className="suit">{props.suit}</span>
    </div>
);

export default Card;